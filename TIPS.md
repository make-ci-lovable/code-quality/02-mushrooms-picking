# Use the report in the next stage

Add this (below) to the job that generates the report:

```yaml
  artifacts:
    paths: [gl-code-quality-report.json]
```

And don't forget this (below) in the next job of the next stage:

```yaml
artifacts:
  reports:
    codequality: gl-code-quality-report.json
```

## Whole example

```yaml
image: node:15.1.0-alpine3.11

stages:
  - code_quality
  - code_quality_check

generate:code:quality:report:
  stage: code_quality
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  artifacts:
    paths: [gl-code-quality-report.json]
    reports:
      codequality: gl-code-quality-report.json
  script:
    - node analyze.js 🌸 mushrooms.txt

check:if:hotspots:
  stage: code_quality_check
  rules:
    - if: $CI_MERGE_REQUEST_IID
  artifacts:
    #paths: [gl-code-quality-report.json]
    reports:
      codequality: gl-code-quality-report.json
  script: |
    cat gl-code-quality-report.json
    content=$(cat gl-code-quality-report.json)
    if [ "${content}" = "[]" ]
    then
      echo "🙂 all good!"
      exit 0
    else
      echo "😡 ouch!"
      #echo "$content" > ./gl-code-quality-report.json
      exit 1
    fi
```
